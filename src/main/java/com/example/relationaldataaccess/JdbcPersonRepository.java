package com.example.relationaldataaccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcPersonRepository implements PersonRepository{

    private JdbcTemplate jdbc;

    @Autowired
    public JdbcPersonRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
    @Override
    public Iterable<Person> findAll() {
        return jdbc.query("select id, first_name, last_name from Person",
                this::mapRowToPerson);
    }
    private Person mapRowToPerson(ResultSet rs, int rowNum)
            throws SQLException {
        return new Person(
                rs.getLong("id"),
                rs.getString("first_name"),
                rs.getString("last_name"));
    }

    @Override
    public Person findByID(Long id) {
        return jdbc.queryForObject("select id, first_name, last_name from Person WHERE id="+id,
                this::mapToPerson);
    }
    private Person mapToPerson(ResultSet rs, int rowNum)
            throws SQLException {
        return new Person(
                rs.getLong("id"),
                rs.getString("first_name"),
                rs.getString("last_name"));
    }

    @Override
    public void save(String firstname, String lastname) {
        jdbc.update("INSERT INTO person(first_name, last_name) VALUES ('"+firstname+"', '"+lastname+"')");
    }
}
