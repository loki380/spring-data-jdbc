package com.example.relationaldataaccess;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PersonRepository {
    Iterable<Person> findAll();
    Person findByID(Long id);
    void save(String firstname, String lastname);

}
