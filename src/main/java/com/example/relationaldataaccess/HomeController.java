package com.example.relationaldataaccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class HomeController {

    JdbcTemplate jdbcTemplate;
    PersonRepository pr;

    @Autowired
    public HomeController(JdbcTemplate jdbcTemplate,PersonRepository pr) {
        this.jdbcTemplate=jdbcTemplate;
        this.pr = pr;
    }

    @GetMapping("/")
    public List<Person> home() {
        List<Person> persons = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT id, first_name, last_name FROM person",
                (rs, rowNum) -> new Person(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"))
        ).forEach(persons::add);

        return persons;
    }
    @GetMapping("/person/{id}")
    public Person person(@PathVariable("id") long id) {
        return pr.findByID(id);
    }
    @PostMapping("/person/new")
    public Iterable<Person> newPerson(String firstname, String lastname) {
        pr.save(firstname,lastname);
        return pr.findAll();
    }
}
