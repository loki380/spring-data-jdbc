//package com.example.relationaldataaccess;
//
//import org.springframework.stereotype.Repository;
//
//import javax.sql.DataSource;
//import java.sql.*;
//import java.util.ArrayList;
//import java.util.List;
//
//@Repository
//public class RawJdbcPersonRepository implements PersonRepository{
//    private DataSource dataSource;
//
//    public RawJdbcPersonRepository(DataSource dataSource) {
//        this.dataSource = dataSource;
//    }
//    @Override
//    public Iterable<Person> findAll() {
//        List<Person> persons = new ArrayList<>();
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        try {
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(
//                    "select id, first_name, last_name from Person");
//            resultSet = statement.executeQuery();
//            while(resultSet.next()) {
//                Person person = new Person(
//                        resultSet.getLong("id"),
//                        resultSet.getString("first_name"),
//                        resultSet.getString("last_name"));
//                persons.add(person);
//            }
//        } catch (SQLException e) {
//            // ??? What should be done here ???
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (SQLException e) {}
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (SQLException e) {}
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (SQLException e) {}
//            }
//        }
//        return persons;
//    }
//
//    @Override
//    public Person findByID(Long id) {
//        Person person = new Person();
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        try {
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(
//                    "select id, first_name, last_name from Person WHERE id="+id);
//            resultSet = statement.executeQuery();
//            while(resultSet.next()) {
//                person.setId(resultSet.getLong("id"));
//                person.setFirstName(resultSet.getString("first_name"));
//                person.setLastName(resultSet.getString("last_name"));
//            }
//        } catch (SQLException e) {
//            System.out.println(e);
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (SQLException e) {}
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (SQLException e) {}
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (SQLException e) {}
//            }
//        }
//        return person;
//    }
//
//    @Override
//    public void save(String firstname, String lastname) {
//        Connection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//        try {
//            connection = dataSource.getConnection();
//            statement = connection.createStatement();
//            statement.executeUpdate("INSERT INTO person(first_name, last_name) VALUES ('"+firstname+"', '"+lastname+"')");
//        } catch (SQLException e) {
//            System.out.println(e);
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (SQLException e) {}
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (SQLException e) {}
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (SQLException e) {}
//            }
//        }
//    }
//}
